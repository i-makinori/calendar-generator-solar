
# edit

1. edit day_form and day_to in `pan_capeldar.py` which defines calendar range

```python
    day_start = datetime.date(2021, 9, 1)
    day_goal   = datetime.date(2022, 3, 31)
# ...
    font_path1 = '' # path to font1 for number cells
    # ...
    font_path2 = '' # path to font2 for text cells
```

1. `$ python pan_calendar.py` in shell to generate pdf calendar
1. `$ ls` to you find pdf to calendar generate


