# -*- coding: utf-8 -*-

import os
import datetime
from dateutil.relativedelta import relativedelta


import cal_array as cala
import cal_array_plot as cala_plot



def last_day_of_month(the_day):
    the_day = the_day + relativedelta(day=31)
    return the_day

def gen_calendar(day_start, day_goal,
                 paper_size='A4', landscape_or_portrait='landscape',
                 margins=(1.00, 1.00, 1.00, 1.00), # (left, top, right, bottom) unit is cm
                 path=os.path.join(os.path.dirname(__file__), 'calendar.pdf')
                 ):
    ## set date
    day_start = day_start.replace(day=1)
    day_goal = last_day_of_month(day_goal)
    # print(day_start)
    # print(day_goal)

    ## make array
    months_array = cala.make_calender(day_start, day_goal)
    # cala.format_array(months_array)

    ## plot data
    # plot_m, fig_m = cala_plot.plot_months_array(months_array)

    plot_m, fig_m = \
        cala_plot.plot_months_array(months_array,
                                    paper_size=paper_size,
                                    landscape_or_portrait=landscape_or_portrait,
                                    margins=margins # (left, top, right, bottom) unit is cm
                                    )

    # plot_m.show()

    ## set path and save
    print(path)
    pp = cala_plot.PdfPages(path)
    pp.savefig(fig_m)
    pp.close()
    

if __name__ == "__main__":
    day_start = datetime.datetime.today().replace(day=1) \
        # the first day of this day's month.
    day_goal = day_start + relativedelta(months=+6, days=-1) \
        # {6 month later and 1 days before} after dati_start.

    ## day_start = datetime.date(2023, 4, 1)
    ## day_goal   = datetime.date(2023, 9, 30)
    
    ## calendar vector
    gen_calendar(day_start, day_goal)



